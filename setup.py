from setuptools import setup, find_packages
import feedgen


def readme():
    with open('README.rst') as f:
        return f.read()

setup(name='feedgen',
      version=feedgen.__version__,
      description='Takes in a list of url and returns a list of files in a directory listing relevant feeds as text '
                  'files',
      url='https://bitbucket.org/akurnya/feedgen/overview',
      author=feedgen.__author__,
      author_email=feedgen.__author_email__,
      classifiers=feedgen.__classifiers__,
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      scripts=['bin/feedgen_rss_parse'],
      install_requires = ['argparse',
                          'feedparser',
                          'urlparse'
                          ])