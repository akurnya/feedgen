import re
import sys
import argparse
import os
import feedfinder
import feedparser
import json
from time import mktime
from datetime import datetime
import url_formalize

"""
:param urls:
Initially you could improvize and build upon this for academic use:
rsslist1 = []
rsslist2 = []
newrss = []
rss = []
feedslist = []
re1='((?:http|https)(?::\\/{2}[\\w]+)(?:[\\/|\\.]?)(?:[^\\s"]*))'   # HTTP URL 1
pat1 = re.compile(re1,re.IGNORECASE|re.DOTALL)
re1='.*?'   # Non-greedy match on filler
re2='((?:http|https)(?::\\/{2}[\\w]+)(?:[\\/|\\.]?)(?:[^\\s"]*))'   # HTTP URL 2
pat2 = re.compile(re1+re2,re.IGNORECASE|re.DOTALL)
#urls = [r'http://www.thefreedictionary.com/_/rss-directory.htm', r'http://newsrack
.in/extras/known-indian-feeds', r'http://dir.yahoo.com/rss/dir/index.php']
htmllist = [urllib2.urlopen(i).read() for i in urls]
for html in htmllist:
	t1 = re.findall(pat1, html)
	t2 = re.findall(pat2, html)
	rsslist1 += t1
	rsslist2 += t2
feeds = rsslist1 + rsslist2
newrss = [i for i in set(feeds) if i not in newrss]
for i in newrss:
	if i.endswith('.xml'):
		rss.append(i)
for i in rss:
	feed = feedparser.parse(i)
	feedslist.append(feed)
return feedslist
"""


def rss_parse(urls):
    urls = url_formalize.url(urls)
	# Non-greedy match on filler
	re1 = '.*?'
	# Uninteresting: word
	re2 = '(?:[a-z][a-z]+)'
	re3 = '.*?'    # Non-greedy match on filler
	re4 = '(?:[a-z][a-z]+)'    # Uninteresting: word
	re5 = '.*?'    # Non-greedy match on filler
	re6 = '((?:[a-z][a-z]+))'    # Word 1
	pattern = re.compile("^http://(.*?)" + (re1 + re2 + re3 + re4 + re5 + re6) + "\.*?", re.IGNORECASE | re.DOTALL)
	parsed_feedsdict = dict()
	feeddict = {re.search(pattern, i).group(): feedfinder.feeds(i) for i in set(urls)}
	"""
	for i in set(urls):
		feeddict.setdefault(re.search(pattern, i).group(), []).append(feedfinder.feeds(i))
	"""
	#print feeddict
	#feeds = [feedfinder.feeds(i) for i in urls]
	for feedkey in feeddict:
		if isinstance(feeddict[feedkey], list):
			for i in feeddict[feedkey]:
				fparse = feedparser.parse(i)
				parsed_feedsdict.setdefault(feedkey, []).append(fparse)
		else:
			fparse = feedparser.parse(feeddict[feedkey])
			parsed_feedsdict.setdefault(feedkey, []).append(fparse)
	#print parsed_feedsdict
	return parsed_feedsdict

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('--ls', nargs='*', type=str, default=[])
	# print sys.argv[1:]
	args = parser.parse_args(sys.argv[1:])
	urls = args.ls
	# print urls
	f = rss_parse(urls)
	path = r'C:\Users\Hero\Desktop\my_pys\feeds\\'
	if os.path.isdir(path) is False:
		os.mkdir(path)
	for x in f.values()[0]:
		for entry in x['entries']:
			with open(path + str(hash(entry['link'])) + '.txt', 'w+') as fin:
				newfeed = {}
				for key in entry:
					if key != "updated_parsed" and key != "published_parsed":
						newfeed[key] = entry[key]
					#print key, entry[key], type(entry[key])
					#print '=================\n'
					elif key == "updated_parsed":
						#pass
						dt = datetime.fromtimestamp(mktime(entry["updated_parsed"]))
						newfeed[key] = str(dt.isoformat())
					else:
						#pass
						dt = datetime.fromtimestamp(mktime(entry["published_parsed"]))
						newfeed[key] = str(dt.isoformat())
				"""
					for key in newfeed:
						print key, newfeed[key]
						print '=======================\n'
					"""
				#print entry
				#print '\n'
				json.dump(newfeed, fin, ensure_ascii=True, indent=4)  # fin.write(json.dumps(entry))
				fin.close()
				"""
					if isinstance(f[feedskey], list):
						for x in f[feedskey]:
							fin.write(str(x))
					else:
						fin.write(str(f[feedskey]))"""