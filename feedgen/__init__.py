__author__ = 'Akul Mathur'
__author_email__ = 'akurnya@gmail.com'
__version__ = '0.1'
__classifiers__ = [
	'Development Status :: 1 - Alpha',
	'Programming Language :: Python :: 2.7.3',
	'Topic :: Text Processing :: Linguistic',
]

import feedfinder
import rss_parse_demo
import url_formalize