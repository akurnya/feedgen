import urlparse

def url(urls):
    for num, i in enumerate(urls):
        o = urlparse.urlparse(i)
        if o.scheme == '':
            if o.path.startswith('www.'):
                newurl = 'http://'+o.path
            else:
                newurl = 'http://www.'+o.path
        urls[num] = newurl
    #print urls
    return urls