feedgen
--------
Takes in a list of url and returns a list of files in a directory listing relevant feeds as text files

To use (with caution), simply do::

    >>> import feedgen
    >>> print feedgen.rss_parse(urls)


